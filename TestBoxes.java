import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestBoxes{
    @Test
    public void testBoxCreate(){
        Box b = new Box();
    }
    /** on veut pouvoir mettre des trucs dedans */
    @Test
    public void testBoxAdd(){
        Box b = new Box();
        Thing truc1 = new Thing("truc1");
        Thing truc2 = new Thing("truc2");
        b.add(truc1);
        b.add(truc2);
    }
    @Test
    public void testBoxContient(){
        Box b = new Box();
        Thing ethan = new Thing("Ethan");
        Thing rol = new Thing("Roland");
        b.add(ethan);
        assertTrue(b.contientTruc(ethan));
        assertFalse(b.contientTruc(rol));
    }
    @Test
    public void testSupprimeTruc(){
        Box b = new Box();
        Thing ethan = new Thing("Ethan");
        Thing rol = new Thing("Roland");
        b.add(rol);
        assertTrue(b.supprimeTruc(rol));
        assertFalse(b.supprimeTruc(ethan));
    }

    @Test
    public void testIsOpen(){
        Box b = new Box(false);
        assertFalse(b.isOpen());
        Box c = new Box(true);
        assertTrue(c.isOpen());
    }

    @Test
    public void testClose(){
        Box b = new Box(true);
        assertTrue(b.isOpen());
        b.close();
        assertFalse(b.isOpen());
    }

    @Test
    public void testOpen(){
        Box b = new Box(false);
        assertFalse(b.isOpen());
        b.open();
        assertTrue(b.isOpen());
    }


    @Test(expected = RuntimeException.class)
    public void testBoxRemoveFail(){
        Thing truc = new Thing("truc");
        Box b = new Box();
        b.remove(truc);
    }


    @Test
    public void testBoxRemoveOk(){
        Thing truc = new Thing ("truc");
        Box b = new Box();
        b.add(truc);
        b.remove(truc);
        assertFalse(b.contientTruc(truc));
    }


    @Test
    public void testActionLook(){
        Thing truc = new Thing("truc");
        Thing truc3 = new Thing("truc3");
        Box b = new Box(true);
        b.add(truc);
        b.add(truc3);
        System.out.println(b.actionLook());
        Thing truc2 = new Thing("truc");
        Box c = new Box(false);
        c.add(truc2);
        System.out.println(c.actionLook());
    }

    @Test
    public void testSetCapacity(){
        Thing truc = new Thing("truc");
        Thing truc3 = new Thing("truc3");
        Box b = new Box(true);
        b.setCapacity(5);
        
    }


    @Test
    public void testCapacity(){
        Thing truc = new Thing("truc");
        Thing truc3 = new Thing("truc3");
        Box b = new Box(true);
        b.setCapacity(5);
        assert b.capacity() == 5;
    }

    @Test
    public void testVolume(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("truc2");
        Box b = new Box(true);
        b.add(truc);
        assert b.getContenu().get(0).volume() == 4;
    }

    @Test
    public void testHasRommFor(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("trucc2");
        Box b = new Box(true);
        b.setCapacity(5);
        assertTrue(b.hasRoomFor(truc));
        assertFalse(b.hasRoomFor(truc2));
    }


    @Test
    public void testActionAddOK(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("trucc2");
        Thing truc3 = new Thing("trucc3");
        Box b = new Box(true);
        b.setCapacity(12);
        b.actionAdd(truc);
        b.actionAdd(truc2);
        assert b.getContenu().size()==2;
    }

    @Test(expected = RuntimeException.class)
    public void testActionAddFail(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("trucc2");
        Thing truc3 = new Thing("trucc3");
        Box b = new Box(true);
        b.setCapacity(12);
        b.actionAdd(truc);
        b.actionAdd(truc2);
        b.actionAdd(truc3);
    }
    
    @Test
    public void testFindOk(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("trucc2");
        Box b = new Box(true);
        b.setCapacity(12);
        b.actionAdd(truc);
        b.actionAdd(truc2);
        assertEquals(b.find("truc"),truc);
    }


    @Test(expected = RuntimeException.class)
    public void testFindFail(){
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("trucc2");
        Box b = new Box(true);
        b.setCapacity(12);
        b.actionAdd(truc);
        b.actionAdd(truc2);
        b.find("truuucc");
    }

    @Test
    public void testBoxFromJsonSimple(){
    Box b = Box.fromJSON("test1.json");
    System.out.println("Boite chargée de capacité :" + b.capacity());
    }


}
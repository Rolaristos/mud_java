import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import com.google.gson.Gson;

/**Classe Thing */
class Thing{
    private String name;

    public Thing(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    @Override
    public String toString(){
        return this.getName();
    }
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Thing){
            return ((Thing)obj).getName().equals(this.name);
        }
        return false;
    }
    public int volume(){
        return this.name.length();
    }
    public void setName(String nom){
        this.name = nom;
    }    
}

/**Classe Box */
public class Box{
    /** Constructeur par défaut avec message à la création */
    private ArrayList<Thing> contents = new ArrayList<Thing>();
    private boolean ouvert;
    private int capacity;
    
    public Box(){
        System.out.println("Box crée");
        this.ouvert = false;
        this.capacity = -1;
    }
    public Box(boolean ouvert){
        this.ouvert = ouvert;
        this.capacity = -1;
        System.out.println("Box crée");
    }
    public Box(int capacity){
        this.capacity = capacity;
        this.ouvert = false;
        System.out.println("Box crée");
    }
    public Box(boolean ouvert, int capacite){
        this.ouvert = ouvert;
        this.capacity = capacite;
        System.out.println("Box crée");
    }
    public void add(Thing chose) {
        this.contents.add(chose);
    }
    public boolean contientTruc(Thing truc){
        return this.contents.contains(truc);
    }
    public boolean supprimeTruc(Thing truc){
        if (this.contientTruc(truc)){
            this.contents.remove(truc);
            return true;
        }
        return false;
    }
    @Override
    public String toString(){
        return "Voici le contenu de votre Box :" + this.contents;
    }
    public boolean isOpen(){
        return this.ouvert;
    }
    public void close(){
        this.ouvert = false;
    }
    public void open(){
        this.ouvert = true;
    }
    public String actionLook(){
        if (this.ouvert){
            return "La boîte contient cela : "+ this.contents;
        }
        return "La boîte est fermée.";
    }
    public void setCapacity(int capacite){
        this.capacity = capacite;
    }
    public int capacity(){
        return this.capacity;
    }
    public void remove(Thing truc) throws RuntimeException {
        boolean ok = this.contents.remove(truc);
        if (!ok) throw new RuntimeException("Remove impossible !");
    }
    public boolean hasRoomFor(Thing t){
        if (this.capacity == -1){
            return true;
        }
        if (this.capacity < t.volume()){
            return false;
        }
        else{
            return true;
        }
    }
    public void actionAdd(Thing t) throws RuntimeException{
        boolean ok = this.hasRoomFor(t);
        if (ok && this.ouvert){
            this.add(t);
            this.capacity -= t.volume();
        }
        else{
            throw new RuntimeException("Ajout impossible !");
        }
    }
    public ArrayList<Thing> getContenu(){
        return this.contents;
    }
    public Thing find(String name) throws RuntimeException {
        if (this.ouvert){
            for (Thing chose : this.contents){
                if (chose.getName() == name){
                    return chose;
                }
            }
        }
        throw new RuntimeException("Vérifier si la boîte est ouverte, ou objet introuvable !");
    }
    public static Box fromJSON(String nom_fichier){
        try {
            FileReader fr = new FileReader("./test1.json");
            Gson json = new Gson();
            Box maBoite = json.fromJson(fr, Box.class);
            System.out.println("Boîte chargée de capacité :" + maBoite.capacity());
            return maBoite;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
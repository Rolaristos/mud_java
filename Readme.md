Équipe constitué de :
RAKOTOMALALA Roland
MARIDAT Ethan

# MUD JAVA

## Création de Box

## Ajout de Thing dans des Box

Les commandes :

    On sauvegarde le fichier et on compile les tests JUnit :

    javac -cp .:junit-4.13.2.jar TestBoxes.java

    javac -cp .:junit-4.13.2.jar:gson-2.10.1.jar TestBoxes.java

    Puis on lance les tests :
    
    java -cp .:junit-4.13.2.jar:hamcrest-2.2.jar org.junit.runner.JUnitCore TestBoxes

    Pour les branches :

    git switch [nom de branche] -> pour changer de branche

    git branch [nom de branche] -> pour créer une branche

    Pour tout le temps se connecter :

    git config --global credential.helper store